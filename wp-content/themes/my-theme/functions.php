<?php
add_theme_support( 'post-thumbnails' );
//function to define the type of custom Case post
function startUpPost() {

    //array of the texts used in the admin menu
    $labels = array(
        'name'               => _x( 'Start-ups', 'post type general name' ),
        'singular_name'      => _x( 'Start-Up', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'Start-Up' ),
        'add_new_item'       => __( 'Add New Start-Up' ),
        'edit_item'          => __( 'Edit Start-Up' ),
        'new_item'           => __( 'New Start-Up' ),
        'all_items'          => __( 'All Start-Ups' ),
        'view_item'          => __( 'View Start-Ups' ),
        'search_items'       => __( 'Search Start-Ups' ),
        'not_found'          => __( 'No Start-Ups found' ),
        'not_found_in_trash' => __( 'No Start-Ups found in the Trash' ), 
        'menu_name'          => 'Start-ups'
    );
    
    //array of post type properties
    $args = array(
        'labels'        => $labels,
        'description'   => 'All Start-ups using the Open-Incubator',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array('editor', 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'has_archive'   => false,
        'taxonomies'    => array( 'category' ),
  
    );
    register_post_type( 'startUp', $args ); 
}

//adds the custom post function to wordpress on init
add_action( 'init', 'startUpPost' );





function whatWeOfferPost() {

    //array of the texts used in the admin menu
    $labels = array(
        'name'               => _x( 'Offers', 'post type general name' ),
        'singular_name'      => _x( 'Offer', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'What We offer' ),
        'add_new_item'       => __( 'Add New Offer' ),
        'edit_item'          => __( 'Edit Offer' ),
        'new_item'           => __( 'New Offer' ),
        'all_items'          => __( 'All Offers' ),
        'view_item'          => __( 'View Offers' ),
        'search_items'       => __( 'Search Offers' ),
        'not_found'          => __( 'No Offers found' ),
        'not_found_in_trash' => __( 'No Offers found in the Trash' ), 
        'menu_name'          => 'Offers'
    );
    
    //array of post type properties
    $args = array(
        'labels'        => $labels,
        'description'   => 'All benefits that the Open-Incubator has to offer.',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array('editor', 'title', 'thumbnail', 'custom-fields', 'page-attributes'),
        'has_archive'   => true,
    );
    register_post_type( 'offer', $args ); 
}

//adds the custom post function to wordpress on init
add_action( 'init', 'WhatWeOfferPost' );










//function to placeholder startups
function createStartUps() {

    $args = array(
        'post_type'      => 'StartUp'
    );

    $posts = new WP_Query($args);

    //quit if there are any placeholders already
    if($posts -> have_posts()) {
        return;
        
    }

    //the amount of placeholders to generate
    $postAmount = 9;

    //generates the predetermined amount of posts
    for ($i = 0; $i <= $postAmount - 1; $i++) {
        $postArg = array(
        'post_ID'       => $i,
        'post_type'     => 'StartUp',
        'post_title'    => "Placeholder Start-Up #{$i}",
        'post_content'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ornare arcu libero, dapibus vulputate urna dapibus quis. Sed molestie erat erat, in pellentesque massa ornare ac. Nunc ultricies porta tortor, eget ornare diam blandit et. Aenean gravida imperdiet risus ut suscipit. Donec ultricies nunc quis velit volutpat, consequat dignissim mauris aliquet. Sed id laoreet orci. Nunc sed faucibus nunc. Ut maximus dui vel sem viverra dignissim.
                            Sed orci metus, lacinia eu felis eget, fermentum faucibus tellus. Vestibulum lectus augue, dapibus eget sagittis quis, pulvinar id nibh. Vivamus fermentum tortor vitae ligula bibendum ultrices. Curabitur ultricies ut magna vel accumsan. Vestibulum et ante cursus leo sodales mollis vel non lorem. Morbi sem nisl, euismod et pellentesque in, rutrum eget risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer eu luctus nibh. Donec ut velit placerat, tempor risus id, hendrerit dui. Proin condimentum est orci, quis rutrum nibh viverra eu. Nam pharetra nunc non nulla convallis malesuada. Morbi elementum lacinia dui, ac posuere quam sodales vel.
                            Fusce interdum metus et eros sollicitudin congue. Quisque accumsan, risus in efficitur tempor, ligula felis facilisis ipsum, quis lobortis orci ex non massa. Aenean non tortor pretium, consequat ante at, pulvinar',
        'post_status'   => 'publish',
        'post_author'   => 1,
        );

        $postID = wp_insert_post($postArg);
    }
}
add_action('init', 'createStartUps');

function createOffers() {
    $args = array(
        'post_type'      => 'offer'
    );

    $posts = new WP_Query($args);

    //quit if there are any placeholders already
    if($posts -> have_posts()) {
        return;
        
    }

    //the amount of placeholders to generate
    $postAmount = 9;

    //generates the predetermined amount of posts
    for ($i = 0; $i <= $postAmount - 1; $i++) {
        $postArg = array(
        'post_ID'       => $i,
        'post_type'     => 'offer',
        'post_title'    => "Placeholder Offer#{$i}",
        'post_content'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ornare arcu libero, dapibus vulputate urna dapibus quis. Sed molestie erat erat, in pellentesque massa ornare ac. Nunc ultricies porta tortor, eget ornare diam blandit et. Aenean gravida imperdiet risus ut suscipit. Donec ultricies nunc quis velit volutpat, consequat dignissim mauris aliquet. Sed id laoreet orci. Nunc sed faucibus nunc. Ut maximus dui vel sem viverra dignissim.
                            Sed orci metus, lacinia eu felis eget, fermentum faucibus tellus. Vestibulum lectus augue, dapibus eget sagittis quis, pulvinar id nibh. Vivamus fermentum tortor vitae ligula bibendum ultrices. Curabitur ultricies ut magna vel accumsan. Vestibulum et ante cursus leo sodales mollis vel non lorem. Morbi sem nisl, euismod et pellentesque in, rutrum eget risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer eu luctus nibh. Donec ut velit placerat, tempor risus id, hendrerit dui. Proin condimentum est orci, quis rutrum nibh viverra eu. Nam pharetra nunc non nulla convallis malesuada. Morbi elementum lacinia dui, ac posuere quam sodales vel.
                            Fusce interdum metus et eros sollicitudin congue. Quisque accumsan, risus in efficitur tempor, ligula felis facilisis ipsum, quis lobortis orci ex non massa. Aenean non tortor pretium, consequat ante at, pulvinar',
        'post_status'   => 'publish',
        'post_author'   => 1,
        );

        $postID = wp_insert_post($postArg);
    }
}
add_action('init', 'createOffers');


function registerMenus() {

    //registers the main menu for use in the header
    register_nav_menus( array(
        'header_menu'   => 'Header menu',
        'footer_menu'   => 'Footer menu'
    ));

    //possible function to register the generated home and posts page in the menu...
}
add_action('init', 'registerMenus');


//add theme options pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));
}

//registers stylesheet
function enqueueStyle() {
	wp_enqueue_style( 'style', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'enqueueStyle' );

?>